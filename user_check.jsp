﻿<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="java.io.*"%>
	<html>
		<script>
	<% request.setCharacterEncoding("UTF-8");
	String directory = application.getRealPath("/UserInfo");
	String user_id = request.getParameter("user_id");
	String user_password = request.getParameter("user_password");
	File dirFile = new File(directory + "/" +user_id+".txt");
	if(!dirFile.isFile()){
		%>
		alert("존재하지 않는 아이디입니다!!");
		location.href='main.jsp';
	<%
	}//사용자의 아이디가 존재하지 않을 경우 경고 창을 띄움
	else{
		FileReader file = new FileReader(directory + "/" +user_id+".txt");
		BufferedReader buffer_reader = new BufferedReader(file);
		String real_password = buffer_reader.readLine();
		String point_num = buffer_reader.readLine();
		String coupon_time = buffer_reader.readLine();
		if(real_password.equals(user_password)){
	%>
			location.href='main.jsp';
	<%
			session.setAttribute("user_id", user_id);
			session.setAttribute("user_password", user_password);
			session.setAttribute("point_num", point_num);
			if(coupon_time != null){
				session.setAttribute("coupon_time",coupon_time);
			}
		}//로그인 성공 시 온라인 상점으로 향하게하고 세션 생성함
		else{
	%>
			alert("패스워드가 틀립니다.");
			location.href='main.jsp';
	<%
		}
	}//아이디에 맞는 패스워드인지 확인
%>
</script>