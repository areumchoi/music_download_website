﻿<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.io.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.*"%>
<!DOCTYPE html>
<html>
	<head>
		<title>Healing my life</title>
		<meta charset="UTF-8">
		<link href="search_music.css" rel="stylesheet" type="text/css"></link>
	</head>
	<body>
		<table>
			<tr>
				<th></th>
				<th>곡명</th>
				<th id="singer">가수명</th>
				<th>발매일</th>
				<th>point</th>
				<th></th>
				<th></th>
			</tr>
			<% request.setCharacterEncoding("UTF-8");
				String directory = application.getRealPath("/source_txt");
				String music_info = request.getParameter("music_info");
				File file = new File(directory);
				String[] file_list = file.list();
				List<String> array = new ArrayList<String>();
				for(int i=0; i<file_list.length; i++){
					if(file_list[i].contains(music_info)){
						array.add(file_list[i]);
					}
				}
				//모든 음악파일과 내가 입력한 값의 문자열을 비교하여 같은 파일의 이름을 배열에 넣음
				if(array.size() == 0){
					out.println("<script language='javascript'>alert('음원을 찾을 수 없습니다.');history.back();</script>");
				}
				else{
					for(int i=0; i< array.size(); i++){
				%>
			<tr>
				<%
					FileReader real_file = new FileReader(directory + "/" +array.get(i));
					BufferedReader buffer_reader = new BufferedReader(real_file);
					String singer = buffer_reader.readLine();
					String nameofsong = buffer_reader.readLine();
					String date_song = buffer_reader.readLine();
					String image = buffer_reader.readLine();
					String music = buffer_reader.readLine();
					String point = buffer_reader.readLine();
					String fileName = array.get(i);
				%>
					<td><img src="<%=image%>"></td>
					<td><%=nameofsong%></td>
					<td><%=singer%></td>
					<td><%=date_song%></td>
					<td><%=point%></td>
					<td>
					<form action="download_music.jsp" method="POST">
						<input type ="hidden" name="song_name" value="<%=fileName%>">
						<input type ="hidden" name="point_value" value="<%=point%>">
						<input type="submit" value="다운 받기">
					</form></td>
					<td>
					<form action="streaming.jsp" method="POST">
						<input type ="hidden" name="streaming_song" value="<%=fileName%>">
						<button id="play">▷</button>
					</form></td>
			</tr>
				<%
					}
				}
				%>	
		</table>
		<button id="back" onclick="location.href='main.jsp'">홈으로 돌아가기</button>
	</body>
</html>