﻿<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.io.*"%>
<% 
	request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html>
<html>
<head>
  <title>Sign up</title>
  <meta charset="UTF-8">
  <link href="sign_up.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
	<div id="body">
		<header>
			<span id="main">Healing my life</span>
		</header>
		<form action="user_sign.jsp" method="GET">
			<p>회원가입</p>
			<span id="id">ID : </span>
			<input type="text" name="new_id" placeholder="아이디를 입력하세요" required> <br>
			<span id="password">PassWord : </span>
			<input type="password" name="new_password" placeholder="비밀번호를 입력하세요" required> <br>
			<input type="submit" id = "sign_up_button" value="가입하기">
			<!--가입버튼-->
		</form>
		<button id="back" onclick="location.href='main.jsp'">뒤로가기</button>
		<!--main page로 돌아가는 버튼-->
	</div>
</body>
</html>