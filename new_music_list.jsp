<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.io.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.*"%>
<% 
	request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html>
<html>
<head>
	<title>최신 음악 chart</title>
	<meta charset="UTF-8">
	<link href="new_music_list.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
	<div>
		<span>최신 음악 List</span><br>
	</div>
	<table>
		<tr>
			<th>Ranking</th>
			<th>Album</th>
			<th>Music</th>
			<th>Singer</th>
			<th>date</th>
		</tr>
		<%
					String directory1 = application.getRealPath("/source_txt");
					File file1 = new File(directory1);
					String[] file_list = file1.list();
					List<String> array_name = new ArrayList<String>();
					List<String> array_singer = new ArrayList<String>();
					List<String> array_img = new ArrayList<String>();
					List<String> array_date = new ArrayList<String>();
					List<String> array_file = new ArrayList<String>();
					
					for(int i=0; i< file_list.length; i++){
						FileReader real_file = new FileReader(directory1 + "/" +file_list[i]);
						BufferedReader buffer_reader = new BufferedReader(real_file);
						String singer = buffer_reader.readLine();
						String nameofsong = buffer_reader.readLine();
						String date_song = buffer_reader.readLine();
						String image = buffer_reader.readLine();
						String music = buffer_reader.readLine();
						String point = buffer_reader.readLine();
						
						if(array_date.size()==0){
							array_name.add(nameofsong);
							array_singer.add(singer);
							array_img.add(image);
							array_date.add(date_song);
							array_file.add(file_list[i]);
						}
						else{
							int x = array_date.size();
							for(int j=0; j<x; j++){
								if(Integer.parseInt(array_date.get(j))<Integer.parseInt(date_song)||Integer.parseInt(array_date.get(j))==Integer.parseInt(date_song)){
									array_date.add(j,date_song);
									array_img.add(j,image);
									array_name.add(j,nameofsong);
									array_singer.add(j,singer);
									array_file.add(j,file_list[i]);
									break;
								}
							}
							if(Integer.parseInt(array_date.get(x-1))>Integer.parseInt(date_song)){
								array_date.add(date_song);
								array_img.add(image);
								array_name.add(nameofsong);
								array_singer.add(singer);
								array_file.add(file_list[i]);
							}
						}
					}//모든 음악파일을 최신 순에 맞춰 배열에 넣음
					for(int k=0; k< array_date.size(); k++){
				%>
				<tr >
					<td><%=k+1%></td>
					<td id="images"><img src="<%=array_img.get(k)%>"></td>
					<td><%=array_name.get(k)%></td>
					<td><%=array_singer.get(k)%></td>
					<td><%=array_date.get(k)%>&nbsp;&nbsp;</td>
					<td>
					<form action="streaming.jsp" method="POST">
						<input type ="hidden" name="streaming_song" value="<%=array_file.get(k)%>">
						<button id="play">▷</button>
					</form></td>
				</tr>
				<%
					}
				%>
			</table>
			<button id="back"onclick="location.href='main.jsp'">홈으로 돌아가기</button>
		</div><!--최신 음악 list-->
	</table>
</body>
</html>