﻿<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.io.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%> 
<%@page import="java.util.*"%>
<% 
	request.setCharacterEncoding("UTF-8");
	String music_info = request.getParameter("streaming_song");
%>
<!DOCTYPE html>
<html>
<head>
	<title>streaming</title>
	<meta charset="UTF-8">
	<link href="streaming.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
		<form name="myform" action="sample_streaming.jsp" method="POST">
			<input type="hidden" name="song_source" value="<%=music_info%>">
		</form>
	<% 
		String user_id = (String)session.getAttribute("user_id");
		String directory1 = application.getRealPath("UserInfo");	
		if(user_id == null){
		%>
		<script>
			alert("로그인이 되어있지 않습니다. 로그인해주세요.");
			location.href="main.jsp";
		</script><!--로그인이 되어있을 않았을 경우 미리듣기나 스트리밍이 불가능하다.-->
		<%
		}
		else{
			FileReader file = new FileReader(directory1 + "/" +user_id+".txt");
			BufferedReader buffer_reader1 = new BufferedReader(file);
			String real_password = buffer_reader1.readLine();
			String point_num = buffer_reader1.readLine();
			String last_time = buffer_reader1.readLine();
			int before = 0;
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			String dateTime = formatter.format(date);
			if(last_time!=null){
				before = Integer.parseInt(last_time);
			}
			int today = Integer.parseInt(dateTime);
			
			if(last_time==null){
		%>
			<script>
				if(confirm("이용권을 구매하지 않으셨습니다. 구매하시겠습니까?")){
					location.href='point_recharge.jsp';
				}
				else{
					document.myform.submit();
				}<!--이용권을 구매하지 않았을 경우 미리듣기가 제공된다.-->
			</script>
		<%
			}
			else if(before<today){
				%>
				<script>
				if(confirm("이용권의 기한이 지났습니다. 이용권을 추가 구매하시겠습니까?")){
					location.href='point_recharge.jsp';
				}
				else{
					window.history.back();
				}<!--이용권의 기한이 지났을 경우 그 이용권은 폐기된다.-->
				</script>
				<%
			}
			else{
				String directory = application.getRealPath("/source_txt");
				PrintWriter writer=null;
				String directory2 = application.getRealPath("UserInfo");
				FileReader file2 = new FileReader(directory2 + "/" +user_id+"_list.txt");
				BufferedReader buffer_reader2 = new BufferedReader(file2);
				String index = buffer_reader2.readLine();
				List<String> array = new ArrayList<String>();
				while( index !=null){
					array.add(index);
					index = buffer_reader2.readLine();
				}
				writer=new PrintWriter(directory2 + "/" +user_id+"_list.txt");
				writer.println(music_info);
				if(array.size()!=0){
					for(int i=0; i<array.size(); i++){
						if(music_info.equals(array.get(i))){
							continue;
						}
						else{
							writer.println(array.get(i));
						}
					}
				}
				writer.close();


						FileReader real_file = new FileReader(directory + "/" +music_info);
						BufferedReader buffer_reader = new BufferedReader(real_file);
						String singer = buffer_reader.readLine();
						String nameofsong = buffer_reader.readLine();
						String date_song = buffer_reader.readLine();
						String image = buffer_reader.readLine();
						String music = buffer_reader.readLine();
						String point = buffer_reader.readLine();
						String root = request.getContextPath();
						String savePath = root+"/source"+music;
						
		%>
		<script>
			var nowTime;
			var totalTime;
			var player = new Audio();

			window.onload = function(){
				startMusic();
			}//창이 켜질 떄 음악을 시작시킴.

			function startMusic(){
				player.src ="<%=savePath%>";
				playMusic();
				startTimeInterval();
			}//음악이 시작되면 가져온 음악파일을 재생시킨다. 이때 interval을 이용해 재생시간을 띄우는 함수를 호출한다.
			function startTimeInterval(){
				id = setInterval( "showPlayTime()", 500 );
			}
			function showPlayTime()
			 {
				nowTime   = Math.floor( player.currentTime ); // 현재 재생시간 추출
				totalTime = Math.floor( player.duration ); // 총 재생시간 추출
				nowtimespan.innerHTML  = changeTimeType( nowTime );
				fulltimespan.innerHTML = changeTimeType( totalTime );
				percentage = Math.floor( ( nowTime / totalTime ) * 100 );//노래 진행 퍼센트 추출
				showProgressBar( percentage ); //퍼센트를 기준으로 진행 bar 나타내는 함수 호출
			 }
			 function showProgressBar( percentage )
			 {
				var elem = document.getElementById("progressspan");
				if(percentage>=100){
					clearInterval(id);//퍼센트가 100을 넘어가면 더이상 재생시간을 진행시키지 않는다.
				}else{
					elem.style.width = percentage+"%";//진행 bar width 지속적 변동
				}
			 }
			 function changeTimeType( time )
			 {
				var result;
				  // 시간이 1분 초과했을 경우
				  if( time >= 60 )
				  {
				   result = Math.floor( time / 60 ) + ": ";
				   if( time % 60 != 0 ) result += time % 60 ;
				  }
				  else
				  {
				   result = "0:"+time ;
				  }
				  
				  return result;
			 }// 현재 진행시간과 노래 시간을 화면에 나타내기위한 convert 함수
			 function playMusic()
			 {
				  player.play();
				  return false;
			 }//노래 재생
			 function pauseMusic()
			{
				player.pause();
			}//노래 일시정지
			 function stopMusic()
			 {
				player.pause();
				player.currentTime= 0;
			 }//노래 중지
			 function forwardMusic()
			 {
				 player.currentTime += 10;
			 }//앞으로 10초 이동
			
			 function reverseMusic()
			 {
				 player.currentTime -= 10;
			 }//뒤로 10초 이동
			 function delete_view(){
				 <%
					FileReader file4 = new FileReader(directory2 + "/" +user_id+"_list.txt");
					BufferedReader buffer_reader4 = new BufferedReader(file4);
					String index3 = buffer_reader4.readLine();
					if(index3 == null){
						%>
						alert("삭제할 리스트가 없습니다!!");
						window.history.back();
						<%
					}
					else{
				 %>
				 
					 document.getElementById('list_music').style.display="none";
					 document.getElementById('delete_list').style.display="block";
					 <%
					}
					 %>
			 }// 노래 list를 제거해주기 위해 라디오 버튼 형태의 list를 view해주는 함수
		</script>
		<table>
			<tr>
				<td id="song_name" colspan = '5'><%=nameofsong%></td>
			</tr>
			<tr  >
				<td id="song_image" colspan = '5'><img src="<%=image%>"></td>
			</tr>
			<tr>
				<td><span id="nowtimespan"></span></td>
				<td colspan='3' id="myBar"><div id="progressspan"></div></td>
				<td><span id="fulltimespan"></span></td>
			</tr>
			<tr id="buttons">
				<td>
					&nbsp;<button onclick="reverseMusic()">◁◁</button>&nbsp;
				</td>
				<td>
					&nbsp;<button onclick="stopMusic()">■</button>&nbsp;
				</td>
				<td>
					&nbsp;<button onclick="playMusic()">▶</button>&nbsp;
				</td>
				<td>
					&nbsp;<button onclick="pauseMusic()">||</button>&nbsp;
				</td>
				<td>
					&nbsp;<button onclick="forwardMusic()">▷▷</button>&nbsp;
				</td>
			</tr>
		</table>
		
		<div id="list_music">
			<span id="list">My List</span>
			<ol>
			<%
							
				FileReader file3 = new FileReader(directory2 + "/" +user_id+"_list.txt");
				BufferedReader buffer_reader3 = new BufferedReader(file3);
				String index2 = buffer_reader3.readLine();
				int i = 1;
					while((index2!=null)){
						FileReader real_file_ = new FileReader(directory + "/" +index2);
						BufferedReader buffer_reader_ = new BufferedReader(real_file_);
						String singer_ = buffer_reader_.readLine();
						String nameofsong_ = buffer_reader_.readLine();
						String date_song_ = buffer_reader_.readLine();
						String image_ = buffer_reader_.readLine();
						String music_ = buffer_reader_.readLine();
						String point_ = buffer_reader_.readLine();
						String root_ = request.getContextPath();
						String savePath_ = root_+"/source"+music_;
			%>
					<form action="streaming.jsp" method="POST">
						<input type ="hidden" name="streaming_song" value="<%=index2%>">
						<button id="list_buttons"><%=i%>.<%=nameofsong_%></button>
					</form>
					<!--노래 목록 list에 대한 부분 클릭시, 다시 재생가능-->
				<%
						index2 = buffer_reader3.readLine();
						i++;
					}
				%>
				</ol>
				<button id="delete_button" onclick="delete_view();">삭제 리스트</button>
		</div>
		<div id="delete_list">
			<span id="lists">My List</span>
			<form action="delete_now.jsp" method="POST">
		<%
				FileReader file5 = new FileReader(directory2 + "/" +user_id+"_list.txt");
				BufferedReader buffer_reader6 = new BufferedReader(file5);
				String index4 = buffer_reader6.readLine();

					while((index4!=null)){
						FileReader real_file_1 = new FileReader(directory + "/" +index4);
						BufferedReader buffer_reader_1 = new BufferedReader(real_file_1);
						String singer_1 = buffer_reader_1.readLine();
						String nameofsong_1 = buffer_reader_1.readLine();
						String date_song_1 = buffer_reader_1.readLine();
						String image_1 = buffer_reader_1.readLine();
						String music_1 = buffer_reader_1.readLine();
						String point_1 = buffer_reader_1.readLine();
						String root_1 = request.getContextPath();
						String savePath_1 = root_1+"/source"+music_1;
			%>	
				<br><input type="radio" name="list_button" value="<%=nameofsong_1%>"><%=nameofsong_1%><br>
				<!--삭제를 원할떄 나타나는 list view-->
				<%
						index4 = buffer_reader6.readLine();
		}
				%>
				<br>
				<button id="delete_it">삭제하기</button>
			</form>
		</div>
		<%
			}
		}
	%>
	<button id="back2" onclick="location.href='main.jsp'">홈으로</button>
</body>
</html>