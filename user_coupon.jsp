﻿<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="java.io.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<html>
	<script>
		<%
			request.setCharacterEncoding("UTF-8");
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			String coupon = request.getParameter("coupon");
			String dateTime = formatter.format(date);
			int endTime = 0;

			String user_id = (String)session.getAttribute("user_id");
			PrintWriter writer=null;
			String directory = application.getRealPath("UserInfo");
			String filePath = directory + "/" +user_id+".txt";
			FileReader file = new FileReader(filePath);
			BufferedReader buffer_reader = new BufferedReader(file);
			String real_password = buffer_reader.readLine();
			String point_num = buffer_reader.readLine();
			String coupon_times = buffer_reader.readLine();
			int real_point = Integer.parseInt(point_num);

			if(coupon.indexOf("30")>=0){
				if( real_point < 1000){
				%>
					alert("포인트가 부족합니다. 충전한 후 사용해주세요");
					location.href='point_recharge.jsp';
				<%
				}//포인트가 부족한 경우 이용권을 구매할수없음
				else{
					if(coupon_times == null){
						endTime = Integer.parseInt(dateTime) + 100;
					}//아직 이용권을 구매한 적이 없을 떄, 현재 시간에서 기한 지정
					else{
						int real_time = ( int )session.getAttribute("coupon_time");
						endTime =real_time + 100;
					}//이미 이용권을 구매한 상태에서 추가 구매시 지정된 기한에서 추가 기한 지정
					real_point -= 1000;
					writer=new PrintWriter(filePath);
					writer.println(real_password);
					writer.println(real_point);
					writer.println(endTime);
					writer.close();
					session.setAttribute("point_num",Integer.toString(real_point));//이용권을 구입하고 남은 포인트 다시 셋팅
					session.setAttribute("coupon_time",endTime);//이용권에 대한 셋팅
					%>
						alert("이용권 구매 완료되었습니다!!");
						location.href="main.jsp";
					<%
				}
			}//30일짜리 이용권 구매시
			else if(coupon.indexOf("60")>=0){
				if( real_point < 1500){
				%>
					alert("포인트가 부족합니다. 충전한 후 사용해주세요");
					location.href='point_recharge.jsp';
				<%
				}
				else{
					if(coupon_times == null){
						endTime = Integer.parseInt(dateTime) + 200;
					}
					else{
						int real_time = ( int )session.getAttribute("coupon_time");
						endTime =real_time  + 200;
					}
					real_point -= 1500;
					writer=new PrintWriter(filePath);
					writer.println(real_password);
					writer.println(real_point);
					writer.println(endTime);
					writer.close();
					session.setAttribute("point_num",Integer.toString(real_point));
					session.setAttribute("coupon_time",endTime);
					%>
						alert("이용권 구매 완료되었습니다!!");
						location.href="main.jsp";
					<%
				}// 모든 코드가 위의 경우와 같음
			}//60일짜리 이용권 구매시
			if(coupon.indexOf("90")>=0){
				if( real_point < 2000){
				%>
					alert("포인트가 부족합니다. 충전한 후 사용해주세요");
					location.href='point_recharge.jsp';
				<%
				}
				else{
					if(coupon_times == null){
						endTime = Integer.parseInt(dateTime) + 300;
					}
					else{
						int real_time = ( int )session.getAttribute("coupon_time");
						endTime =real_time  + 300;
					}
					real_point -= 2000;
					writer=new PrintWriter(filePath);			
					writer.println(real_password);
					writer.println(real_point);
					writer.println(endTime);
					writer.close();
					session.setAttribute("point_num",Integer.toString(real_point));
					session.setAttribute("coupon_time",endTime);
					%>
						alert("이용권 구매 완료되었습니다!!");
						location.href="main.jsp";
					<%
				}//모든 코드가 위의 경우와 같음
			}//90일짜리 이용권 구매시
			
		%>
	</script>
</html>