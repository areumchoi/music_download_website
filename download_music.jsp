﻿<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.io.*"%>
<%@ page import="java.text.*" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.net.*" %>
	<html>
		<script>
		<%
			request.setCharacterEncoding("UTF-8");
			if(session.getAttribute("user_id")==null){
			%>
				alert("로그인 후 이용해주세요.");
				window.history.back();
			<%
			}//로그인을 하지 않았을 경우
			else{
				String directory = application.getRealPath("/source_txt");
				String song_name = request.getParameter("song_name");
				String point_value = request.getParameter("point_value");
				int point_ = Integer.parseInt(point_value);
				String real_point =(String)session.getAttribute("point_num");
				int real_point_ = Integer.parseInt(real_point);
				if(real_point_<point_){
				%>
					if(confirm("포인트가 부족합니다. 충전하시겠습니까?")){
						location.href='point_recharge.jsp';
					}
					else{
						window.history.back();
					}
				<%
				}//다운로드할 포인트가 부족할 경우
				else{
					FileReader real_file = new FileReader(directory + "/" +song_name);
					BufferedReader buffer_reader = new BufferedReader(real_file);
					String singer = buffer_reader.readLine();
					String real_song_name = buffer_reader.readLine();
					String date_song = buffer_reader.readLine();
					String image = buffer_reader.readLine();
					String music = buffer_reader.readLine();
					String root = request.getSession().getServletContext().getRealPath("/");//현재의 절대 루트 경로
					String savePath = root+ "source";//다운받을 파일 경로
					String filename = music;//다운받을 파일
					String orgfilename = filename ;//다운받을 떄의 파일의 이름 지정
					
					InputStream input = null;
					OutputStream os = null;
					File file = null;
					boolean skip = false;
					String client = "";

					try{
						// 파일을 읽어 스트림에 담기
						try{
							file = new File(savePath,filename);//다운받을 파일 지정
							input = new FileInputStream(file);
						}catch(FileNotFoundException fe){
							skip = true;
						}//파일을 찾지 못할 경우 예외처리
						
						client = request.getHeader("User-Agent");

						// 파일 다운로드 헤더 지정
						response.reset() ;
						response.setContentType("application/octet-stream");
						response.setHeader("Content-Description", "JSP Generated Data");


						if(!skip){
							 if(client.indexOf("MSIE") != -1){
								response.setHeader ("Content-Disposition", "attachment; filename="+new String(orgfilename.getBytes("KSC5601"),"ISO8859_1"));
							 }else{
							orgfilename = new String(orgfilename.getBytes("utf-8"),"iso-8859-1");//다운받을 파일의 이름을 한글로 변환
							response.setHeader("Content-Disposition", "attachment; filename=\"" + orgfilename + "\"");
							response.setHeader("Content-Type", "application/octet-stream; charset=utf-8");
							 }
							response.setHeader ("Content-Length", ""+file.length() );
						   
							os = response.getOutputStream();
							byte b[] = new byte[(int)file.length()];//용량 지정
							int leng = 0;
							
							while( (leng = input.read(b)) > 0 ){
								os.write(b,0,leng);
							}

						}//파일을 찾았을 경우
						else{
						 response.setContentType("text/html;charset=UTF-8");
						 %>
							alert("음원을 찾을 수 없습니다.");
							location.href="main.jsp";
						<%	
						}//파일을 찾지 못했을 경우
						
						input.close();
						os.close();

					}catch(Exception e){
					  e.printStackTrace();
					}
					String directory_user = application.getRealPath("/UserInfo");
					String user_id = (String)session.getAttribute("user_id");
					String filePath_user = directory_user + "/" +user_id+".txt";
					FileReader file_user = new FileReader(filePath_user);
					BufferedReader buffer_reader_user = new BufferedReader(file_user);
					String real_password = buffer_reader_user.readLine();
					int totalNum = (real_point_-point_);
					PrintWriter writer_user=new PrintWriter(filePath_user);
					writer_user.println(real_password);
					writer_user.println(totalNum);
					writer_user.close();
					session.setAttribute("point_num",Integer.toString(totalNum));//다운로드로 차감된 포인트 셋팅
				}
			}
				%>
	</script>
</html>