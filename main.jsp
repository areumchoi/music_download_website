﻿<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.io.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.*"%>
<% 
	request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html>
<html>
<head>
	<title>Healing my life</title>
	<meta charset="UTF-8">
	<link href="main.css" rel="stylesheet" type="text/css"></link>
</head>
<body>

	<header>
		<form action="search_music.jsp" method="POST" id="search_form">
			<span>Healing my life</span>
			<input type ="text" id="music_name" name="music_info">
		</form>
		<!--search버튼-->
	</header>
	<div id="music">
		<div id="new_music">
			<button onclick="location.href='new_music_list.jsp'">최신 음악 </button>
			<table id="new_10">
				<tr>
					<th>Ranking</th>
					<th>Album</th>
					<th>Music</th>
					<th>Singer</th>
					<th>date</th>
				</tr>
				<%
					String directory1 = application.getRealPath("/source_txt");
					File file1 = new File(directory1);
					String[] file_list = file1.list();
					List<String> array_name = new ArrayList<String>();
					List<String> array_singer = new ArrayList<String>();
					List<String> array_img = new ArrayList<String>();
					List<String> array_date = new ArrayList<String>();
					List<String> array_file = new ArrayList<String>();
					
					for(int i=0; i< file_list.length; i++){
						FileReader real_file = new FileReader(directory1 + "/" +file_list[i]);
						BufferedReader buffer_reader = new BufferedReader(real_file);
						String singer = buffer_reader.readLine();
						String nameofsong = buffer_reader.readLine();
						String date_song = buffer_reader.readLine();
						String image = buffer_reader.readLine();
						String music = buffer_reader.readLine();
						String point = buffer_reader.readLine();
						
						if(array_date.size()==0){
							array_name.add(nameofsong);
							array_singer.add(singer);
							array_img.add(image);
							array_date.add(date_song);
							array_file.add(file_list[i]);
						}
						else{
							int x = array_date.size();
							for(int j=0; j<x; j++){
								if(Integer.parseInt(array_date.get(j))<Integer.parseInt(date_song)||Integer.parseInt(array_date.get(j))==Integer.parseInt(date_song)){
									array_date.add(j,date_song);
									array_img.add(j,image);
									array_name.add(j,nameofsong);
									array_singer.add(j,singer);
									array_file.add(j,file_list[i]);
									break;
								}
							}
							if(Integer.parseInt(array_date.get(x-1))>Integer.parseInt(date_song)){
								array_date.add(date_song);
								array_img.add(image);
								array_name.add(nameofsong);
								array_singer.add(singer);
								array_file.add(file_list[i]);
							}
						}
					}
					for(int k=0; k< 5; k++){
				%>
				<tr >
					<td><%=k+1%></td>
					<td id="images"><img src="<%=array_img.get(k)%>"></td>
					<td><%=array_name.get(k)%></td>
					<td><%=array_singer.get(k)%></td>
					<td><%=array_date.get(k)%>&nbsp;&nbsp;</td>
					<td>
					<form action="streaming.jsp" method="POST">
						<input type ="hidden" name="streaming_song" value="<%=array_file.get(k)%>">
						<button id="play">▷</button>&nbsp;&nbsp;
					</form></td>
				</tr>
				<%
					}
				%>
			</table>
		</div><!--최신 음악 list-->
	</div>
	<div id="user">
	<%
		if(session.getAttribute("user_id")==null){
	%>
	<div id="user_log_in">
		<form action="user_check.jsp" method="POST">
			<div id="input">
				<input type="text" name="user_id" placeholder="ID"> <br>
				<input type="password" name="user_password" placeholder="PassWord"> <br>	
			</div>
			<div id="submit">
				<input type="submit" id = "on_line" value="로그인">		
			</div>
		</form>
		<button id = "sign_up" onclick="location.href='sign_up.jsp'">회원가입</button>
	</div>
	<%
		}
		else{
			String directory = application.getRealPath("/UserInfo");
			FileReader file = new FileReader(directory+"/"+session.getAttribute("user_id")+".txt");
			BufferedReader buffer_reader = new BufferedReader(file);
			String real_password = buffer_reader.readLine();
			String point_num = buffer_reader.readLine();
	%>
	<!--login창-->
	<div id="user_information">
		<span id="my_info">My Information</span><br>
		<p id="id">ID: <%=session.getAttribute("user_id")%></p>
		<p id="point">포인트: <%=point_num%></p><!--이용권이 존재하지 않는 경우 이용권 구매 창으로 넘어가도록 설정-->
		<div id = "buttons">	
			<button id="buy_point" onclick="location.href='point_recharge.jsp'">이용권/포인트</button>
			<button id="log_out" onclick="location.href='log_out.jsp'">로그아웃</button>
		</div>
	</div>
	<!--로그인 완료시 뜨는 창-->
	<%
		}
	%>
	<div id="recommend_div">
		<span id="recommend_span">How old are you?</span><br>
		<span id="age_recommend">당신의 나이에 맞게 음악을 추천해드립니다</span>
		<form action ="how_old.jsp" method="POST">
			<input type="text" name="my_age">
		</form>
	</div>
</div>
</body>
</html>