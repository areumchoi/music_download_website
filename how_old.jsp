﻿<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.io.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.*"%>
<html>
	<head>
		<title>Healing my life</title>
		<meta charset="UTF-8">
		<link href="how_old.css" rel="stylesheet" type="text/css"></link>
	</head>
	<body>
		<%
			request.setCharacterEncoding("UTF-8");
			String real_age = request.getParameter("my_age");
			int my_age = Integer.parseInt(real_age);
			String directory = application.getRealPath("/source_txt");
			File file = new File(directory);
			String[] file_list = file.list();
			List<String> array_name = new ArrayList<String>();
			List<String> array_singer = new ArrayList<String>();
			List<String> array_img = new ArrayList<String>();
			List<String> array_date = new ArrayList<String>();
			List<String> array_file = new ArrayList<String>();

			for(int i=0; i< file_list.length; i++){
				FileReader real_file = new FileReader(directory + "/" +file_list[i]);
				BufferedReader buffer_reader = new BufferedReader(real_file);
				String singer = buffer_reader.readLine();
				String nameofsong = buffer_reader.readLine();
				String date_song = buffer_reader.readLine();
				String image = buffer_reader.readLine();
				String music = buffer_reader.readLine();
				String point = buffer_reader.readLine();
				
				if(array_date.size()==0){
					array_name.add(nameofsong);
					array_singer.add(singer);
					array_img.add(image);
					array_date.add(date_song);
					array_file.add(file_list[i]);
				}
				else{
					int x = array_date.size();
					for(int j=0; j<x; j++){
						if(Integer.parseInt(array_date.get(j))<Integer.parseInt(date_song)||Integer.parseInt(array_date.get(j))==Integer.parseInt(date_song)){
							array_date.add(j,date_song);
							array_img.add(j,image);
							array_name.add(j,nameofsong);
							array_singer.add(j,singer);
							array_file.add(j,file_list[i]);
							break;
						}
					}	
					if(Integer.parseInt(array_date.get(x-1))>Integer.parseInt(date_song)){
						array_date.add(date_song);
						array_img.add(image);
						array_name.add(nameofsong);
						array_singer.add(singer);
						array_file.add(file_list[i]);
					}
				}
			}//모든 노래 파일을 최신 순으로 나열
			int young = 6;
			int adult = young*2;
			int old = young*3;
			int n = 0;
			
			if(my_age>=0&&my_age<30){
				n = (int) (Math.random() * (young-1));
			}
			else if(my_age>=30&&my_age<50){
				n = (int) (Math.random() * (adult-1))+young ;
			}
			else if(my_age>=50){
				n= (int) (Math.random() * (old-1))+adult;
			}
			//내 나이의 경우에 따라 랜덤한 숫자 뽑아냄
			else{
			%>
			<script>
				alert("나이 값이 이상합니다. 다시 입력해주세요!!");
				location.href="main.jsp";
			</script>
			<%			
			}//나이 값이 음수일 경우
		%>
			<div id="recommend">
				<span>이런 음악은 어때요??</span><br>
				<form action="streaming.jsp" method="POST">
						<input type ="hidden" name="streaming_song" value="<%=array_file.get(n)%>">
						<button id="images"><img src="<%=array_img.get(n)%>"></button>
				</form>
				<p><%=array_name.get(n)%></p>
				<p><%=array_singer.get(n)%></p>
			</div>
			<!--랜덤하게 노래를 추천해주고 이미지를 클릭할 경우 스트리밍이 가능하도록 함-->
			<button id="back" onclick="location.href='main.jsp'">돌아가기</button>
	</body>
</html>