﻿<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.io.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%> 
<%@page import="java.util.*"%>
<% 
	request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html>
<html>
<head>
	<title>sample_streaming</title>
	<meta charset="UTF-8">
	<link href="streaming.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
	<% request.setCharacterEncoding("UTF-8");
		String user_id = (String)session.getAttribute("user_id");
		
		String directory = application.getRealPath("/source_txt");
		String music_info = request.getParameter("song_source");


		FileReader real_file = new FileReader(directory + "/" +music_info);
		BufferedReader buffer_reader = new BufferedReader(real_file);
		String singer = buffer_reader.readLine();
		String nameofsong = buffer_reader.readLine();
		String date_song = buffer_reader.readLine();
		String image = buffer_reader.readLine();
		String music = buffer_reader.readLine();
		String point = buffer_reader.readLine();
		String root = request.getContextPath();
		String savePath = root+"/source"+music;
						
		%>
		<script>
			var nowTime;
			var totalTime;
			var player = new Audio();
		//미리듣기를 위한 플레이어이다. 일반 스트리밍 플레이어와 비슷하지만 다른 점은 재생시간 추출부분쪽에 있다.
			window.onload = function(){
				startMusic();
			}

			function startMusic(){
				player.src ="<%=savePath%>";
				playMusic();
				startTimeInterval();
			}
			function startTimeInterval(){
				id = setInterval( "showPlayTime()", 500 );
			}
			function showPlayTime()
			 {
				nowTime   = Math.floor( player.currentTime ); 
				totalTime = Math.floor( 60 ); // 총 재생시간을 1분으로 제한
				nowtimespan.innerHTML  = changeTimeType( nowTime );
				fulltimespan.innerHTML = changeTimeType( totalTime );
				percentage = Math.floor( ( nowTime / totalTime ) * 100 );
				showProgressBar( percentage ); 
			 }
			 function showProgressBar( percentage )
			 {
				var elem = document.getElementById("progressspan");
				if(percentage>=100){
					clearInterval(id);
					stopMusic();//총 재생시간이 1분을 넘기면 음악을 멈추도록 함.
				}else{
					elem.style.width = percentage+"%";
				}
			 }
			 function changeTimeType( time )
			 {
				var result;
				result = "0:"+time ;
				return result;
			 }
			 function playMusic()
			 {
				  if(player.currentTime>60){
					  stopMusic();
				  }//현재 재생시간이 60초가 넘어가면 더이상 play되지 않음.
				  else{
					player.play();
				  }
			 }
			 function pauseMusic()
			{
				player.pause();
			}
			 function stopMusic()
			 {
				player.pause();
				player.currentTime= 0;
			 }
			 function forwardMusic()
			 {
				 player.currentTime += 10;
			 }
			
			 function reverseMusic()
			 {
				 player.currentTime -= 10;
			 }
					</script>
		<p id="sample">Sample Streaming</p>
		<table>
			<tr>
				<td id="song_name" colspan = '5'><%=nameofsong%></td>
			</tr>
			<tr  >
				<td id="song_image" colspan = '5'><img src="<%=image%>"></td>
			</tr>
			<tr>
				<td><span id="nowtimespan"></span></td>
				<td colspan='3' id="myBar"><div id="progressspan"></div></td>
				<td><span id="fulltimespan"></span></td>
			</tr>
			<tr id="buttons">
				<td>
					&nbsp;<button onclick="reverseMusic()">◁◁</button>&nbsp;
				</td>
				<td>
					&nbsp;<button onclick="stopMusic()">■</button>&nbsp;
				</td>
				<td>
					&nbsp;<button onclick="playMusic()">▶</button>&nbsp;
				</td>
				<td>
					&nbsp;<button onclick="pauseMusic()">||</button>&nbsp;
				</td>
				<td>
					&nbsp;<button onclick="forwardMusic()">▷▷</button>&nbsp;
				</td>
			</tr>
		</table><br>
		<button id="back" onclick="window.history.back()">이전 페이지로</button>
	</body>
</html>