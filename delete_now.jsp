﻿<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.io.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%> 
<%@page import="java.util.*"%>
<% 
	request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html>
<html>
	<script>
		<%
		String user_id = (String)session.getAttribute("user_id");
		String directory = application.getRealPath("UserInfo");	
		String list_button = request.getParameter("list_button");	
	
		FileReader file = new FileReader(directory + "/" +user_id+"_list.txt");
		BufferedReader buffer_reader = new BufferedReader(file);
		List<String> array = new ArrayList<String>();
		String index = buffer_reader.readLine();
		
		if(list_button == null){
			%>
			alert("삭제할 값이 선택되지 않았습니다.");
			window.history.back();
			<%
		}
		else{
			while(index != null){
				if(index.contains(list_button)){
					index = buffer_reader.readLine();
				}//인덱스에 내가 삭제하고자 하는 문자열이 존재하는 경우 배열에 넣지말고 넘긴다. (삭제할 내용이므로)
				else{

					array.add(index);
					index = buffer_reader.readLine();
				}
			}
			buffer_reader.close();
			PrintWriter writer=null;
			writer=new PrintWriter(directory + "/" +user_id+"_list.txt");
			for(int j = 0; j< array.size(); j++){
				writer.println(array.get(j));
			}//배열에 들어가있는 값들만 모아서 쓴다.
			writer.close();

		
			%>
			alert("삭제 완료되었습니다.");
			window.location.reload(true);
			<!--단순히 삭제후 메인 페이지로 돌아가면, 페이지가 재로딩이 된 것이 아니라 삭제가 되었음에도 삭제가 안된 것 처럼 나타나게됨. 재 로딩 후, 메인 페이지로 돌아가도록 조취함-->
			location.href='main.jsp';
		<%
		}
		%>
</script>
</html>